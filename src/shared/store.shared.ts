import { createContext } from 'react';

const initialStoreValue = {
	items: []
};

export const AppContext = createContext([ initialStoreValue, null ]);
