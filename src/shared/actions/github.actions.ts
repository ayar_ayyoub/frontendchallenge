import axios, { AxiosRequestConfig } from 'axios';
import { FETCH_REPOSITORIES } from './actions.types';

const API_ENDPOINT = '/repos?path=/search/repositories?q=created:>2017-10-22&sort=stars&order=desc';
export const fetchRepositories = (dispatch: any, daysNoAgo?: number, page?: number, size?: number) => {
	const config = {
		headers: { 'Access-Control-Allow-Origin': '*' }
	};

	axios
		.get(API_ENDPOINT) //'/repos?path=/search/repositories?q=created:>2017-10-22&sort=stars&order=desc')
		.then((resp) => {
			console.log(resp);
			dispatch({
				type: FETCH_REPOSITORIES,
				payload: [ 'Hello', 'world' ]
			});
		})
		.catch((err) => console.log(err));
};
