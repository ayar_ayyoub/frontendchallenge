import React, { useReducer } from 'react';
import { FETCH_REPOSITORIES } from '../actions/actions.types';

const initialStoreValue = {
	items: []
};

const reducer = (state: any, action: any) => {
	switch (action.type) {
		case FETCH_REPOSITORIES:
			return {
				...state,
				items: action.payload
			};
		default:
			return state;
	}
};

export default () => useReducer(reducer, initialStoreValue);
