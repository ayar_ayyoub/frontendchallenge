import axios from 'axios';

const TIMEOUT = 1 * 60 * 1000;
const SERVER_API_URL = 'http://localhost:8080/github/languages/api'; //'https://api.github.com';

axios.defaults.baseURL = SERVER_API_URL;
axios.defaults.timeout = TIMEOUT;

const setupAxiosInterceptors = (onUnauthenticated: Function) => {
	const onRequestSuccess = (config: any) => {
		config.headers = {
			'Access-Control-Allow-Origin': '*'
		};

		return config;
	};

	const onResponseSuccess = (response: any) => response;
	const onResponseError = (err: any) => {
		onUnauthenticated();

		return Promise.reject(err);
	};
	axios.interceptors.request.use(onRequestSuccess);
	axios.interceptors.response.use(onResponseSuccess, onResponseError);
};

export default setupAxiosInterceptors;
