import React, { useEffect } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonRouterOutlet } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import Home from './pages/Home';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import setupAxiosInterceptors from './configs/axiosInterceptors.config';
import { AppContext } from './shared/store.shared';
import ErrorBoundary from './components/sharedComponents/ErrorBoundary.component';
import githubReducer from './shared/reducers/GithubReducer.reducer';

const App: React.FC = () => {
	useEffect(() => {
		console.log('Setting Axios Interceptors');
		setupAxiosInterceptors(() => {
			console.log('Connexion Erreur Handling');
		});
	}, []);

	return (
		<AppContext.Provider value={githubReducer()}>
			<ErrorBoundary>
				<IonApp>
					<IonReactRouter>
						<IonRouterOutlet>
							<Route path="/home" component={Home} exact={true} />
							<Route exact path="/" render={() => <Redirect to="/home" />} />
						</IonRouterOutlet>
					</IonReactRouter>
				</IonApp>
			</ErrorBoundary>
		</AppContext.Provider>
	);
};

export default App;
